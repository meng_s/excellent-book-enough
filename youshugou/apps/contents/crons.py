# import sys
# import os
# import django
#
#
#
# # !/usr/bin/env python
# sys.path.insert(0, '../../')
#
# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "youshugou.settings")
#
# django.setup()


"""提供首页广告界面静态渲染"""
from apps.contents.models import ContentCategory
from celery_tasks.main import app
from utils.goods import get_categories
from youshugou import settings

@app.task
def generic_index():
    import time
    print("-----------模板渲染中%s-----" % time.ctime())
    categories = get_categories()
    contents = {}
    content_categories = ContentCategory.objects.all()
    for cat in content_categories:
        contents[cat.key] = cat.content_set.filter(status=True).order_by('sequence')

    # 渲染模板的上下文
    context = {
        'categories': categories,
        'contents': contents,
    }
    from django.template import loader
    # 1. 加载渲染的模板
    index_template = loader.get_template('index.html')
    # 2. 把数据给模板
    index_html_data = index_template.render(context)
    # 3. 把渲染好的写入到指定文件
    import os
    file_path = os.path.join(os.path.dirname(settings.BASE_DIR), 'front_end_pc/index.html')
    with open(file_path, 'w', encoding='utf-8') as f:
        f.write(index_html_data)