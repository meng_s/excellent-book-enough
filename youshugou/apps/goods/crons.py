import sys
import os
import django



# !/usr/bin/env python
sys.path.insert(0, '../../')

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "youshugou.settings")

django.setup()
# 不可更改导入位置
from apps.goods.models import SKU
from utils.goods import get_categories, get_goods_specs, get_breadcrumb
from youshugou import settings


def generic_detail_html(sku):
    """提供商品详情页"""
    # 获取当前sku的信息
    # try:
    #     sku = SKU.objects.get(id=sku_id)
    # except SKU.DoesNotExist:
    #     return render(request, '404.html')

    # 查询商品频道分类
    categories = get_categories()
    # 查询面包屑导航
    breadcrumb = get_breadcrumb(sku.category)
    goods_specs = get_goods_specs(sku)
    # 渲染页面
    context = {
        'categories': categories,
        'breadcrumb': breadcrumb,
        'sku': sku,
        'specs': goods_specs,
    }
    '''
    <SPUSpecification: Apple MacBook Pro 笔记本: 屏幕尺寸>,
     <SPUSpecification: Apple MacBook Pro 笔记本: 颜色>, 
     <SPUSpecification: Apple MacBook Pro 笔记本: 版本>]>
    '''
    from django.template import loader
    detail_template = loader.get_template('detail.html')
    detail_html_data = detail_template.render(context)
    import os
    file_path = os.path.join(os.path.dirname(settings.BASE_DIR), 'front_end_pc/goods/%s.html' % sku.id)
    with open(file_path, 'w', encoding='utf-8') as f:
        f.write(detail_html_data)
    print(sku.id)


skus = SKU.objects.all()
for sku in skus:
    generic_detail_html(sku)
