from django.urls import path

from apps.oauth.views import QQLoginTool, OauthQQview

urlpatterns = [
    path("qq/authorization/", QQLoginTool.as_view()),
    path("oauth_callback/", OauthQQview.as_view()),
]
