from itsdangerous import TimedJSONWebSignatureSerializer as Serializer

from youshugou import settings


def Serializer_openid(openid):
    s = Serializer(secret_key=settings.SECRET_KEY, expires_in=3600)
    return s.dumps(openid).decode()


def UnSerializer_openid(openid):
    s = Serializer(secret_key=settings.SECRET_KEY, expires_in=3600)
    try:
        result = s.loads(openid)
    except Exception:
        return None
    else:
        return result
