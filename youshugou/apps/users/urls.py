from django.urls import path

from apps.users.views import UsernameCountView, RegisterView, MobileCountView, LoginView, LogoutView, InfoView, \
    EmailView, EmailVerifyView, AddressCreate, AddressView, UpdateDestroyAddressView, UpdateTitleAddressView, \
    DefaultAddressView, ChangePasswordView, UserHistoryView

urlpatterns = [
    # 判断用户名是否重复
    # path('/usernames/<username>/count/', UsernameCount.as_view()),
    # 注意开头不能有'/' 否则会造成无法访问
    path('usernames/<username:username>/count/', UsernameCountView.as_view()),
    # 判断手机号是否重复
    path('mobiles/<mobile:mobile>/count/', MobileCountView.as_view()),
    path('register/', RegisterView.as_view()),
    path('login/', LoginView.as_view()),
    path('logout/', LogoutView.as_view()),
    path('info/', InfoView.as_view()),
    path('emails/', EmailView.as_view()),
    path('emails/verification/', EmailVerifyView.as_view()),
    path('addresses/', AddressView.as_view()),
    path('addresses/create/', AddressCreate.as_view()),
    path('addresses/<int:address_id>/', UpdateDestroyAddressView.as_view()),
    path('addresses/<int:address_id>/default/', DefaultAddressView.as_view()),
    path('addresses/<int:address_id>/title/', UpdateTitleAddressView.as_view()),
    path('password/', ChangePasswordView.as_view()),
    path('browse_histories/', UserHistoryView.as_view()),
]
