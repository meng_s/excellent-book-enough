from itsdangerous import TimedJSONWebSignatureSerializer as Serializer

from youshugou import settings


def Serializer_email_verify_token(user_id):
    s = Serializer(secret_key=settings.SECRET_KEY, expires_in=3600)
    user_id = s.dumps(user_id)
    return user_id.decode()


def UnSerializer_email_verify_token(user_id):
    s = Serializer(secret_key=settings.SECRET_KEY, expires_in=3600)
    try:
        result = s.loads(user_id)
    except Exception:
        return None
    else:
        return result
