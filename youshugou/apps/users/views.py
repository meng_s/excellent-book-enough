import re
from asyncio.log import logger

from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.contrib.auth import login, authenticate, logout
from django.views import View
from django_redis import get_redis_connection

from apps.carts.utils import merge_cart_cookie_to_redis
from apps.goods.models import SKU
from apps.users.models import User, Address
from apps.users.utils import Serializer_email_verify_token, UnSerializer_email_verify_token
from celery_tasks.email.tasks import send
from utils.models import BaseModel
from utils.transformJson import TransformJson  # 字符串转换为Json

from utils.views import LoginRequiredJSONMixin


# 用户名重复判断
class UsernameCountView(View):
    def get(self, request, username):
        count = User.objects.filter(username=username).count()
        return JsonResponse({'code': 0, 'count': count, 'errmsg': 'ok'})


# 手机号重复判断
class MobileCountView(View):
    def get(self, request, mobile):
        count = User.objects.filter(mobile=mobile).count()
        return JsonResponse({'code': 0, 'count': count, 'errmsg': 'ok'})


# 注册模块
class RegisterView(View):
    def post(self, request):
        body_dict = TransformJson(request)
        username = body_dict.get('username')
        password = body_dict.get('password')
        password2 = body_dict.get('password2')
        mobile = body_dict.get('mobile')
        allow = body_dict.get('allow')
        sms_code = body_dict.get('sms_code')
        # 获取手机验证码
        redis_conn = get_redis_connection('code')
        sms_code_server = redis_conn.get(mobile)
        # 验证数据(由于前端已经验证，此时的注册失败为非法请求，判断失败时选择是否直接禁用ip)
        if not all([username, password, password2, mobile, allow]):
            return JsonResponse({'code': 400, 'errmsg': '参数不全'})
        elif not re.match('[a-zA-Z0-9_-]{5,20}', username):  # 用户名验证
            return JsonResponse({'code': 400, 'errmsg': '用户名不满足规则'})
        elif not (password == password2):
            return JsonResponse({'code': 400, 'errmsg': '两次输入密码不一致'})
        elif not (16 < len(password2) + len(password) < 40):  # python 可以使用类似数学的判断
            return JsonResponse({'code': 400, 'errmsg': '密码长度不正确'})
        elif not re.match('^1[345789]\\d{9}$', mobile):
            return JsonResponse({'code': 400, 'errmsg': '手机号格式不正确'})
        # 验证手机验证码
        elif sms_code_server is None:
            return JsonResponse({'code': 400, 'errmsg': '短信验证码以过期'})
        # elif not (sms_code_server.decode() == str(sms_code)):
        elif not ('1111' == str(sms_code)):
            return JsonResponse({'code': 400, 'errmsg': '验证码不正确'})
        elif not allow:  # 是否勾选用户协议
            return JsonResponse({'code': 400, 'errmsg': '未同意协议'})
        # 新增数据
        else:
            user = User.objects.create_user(username=username, password=password, mobile=mobile)
            user.save()
            # 登录用户信息保存
            login(request, user)
            return JsonResponse({'code': 0, 'message': 'OK'})


# 登录模块
class LoginView(View):
    def post(self, request):
        data = TransformJson(request)
        username = data.get('username')
        password = data.get('password')
        remembered = data.get('remembered')
        if not all([username, password]):
            return JsonResponse({'code': 400, 'errmsg': '参数不全'})
        # 实现手机号或用户名登录的选择
        if re.match('1[345789]\\d{9}', username):
            User.USERNAME_FIELD = 'mobile'
        else:
            User.USERNAME_FIELD = 'username'
        # 传递用户名和密码，正确返回用户信息，错误返回None
        user = authenticate(username=username, password=password)
        if user is None:
            return JsonResponse({'code': 400, 'errmsg': '账号或密码错误'})
        login(request, user)
        # 判断是否记住登录
        if remembered is not None:
            request.session.set_expiry(None)
        else:
            request.session.set_expiry(0)
        # 首页显示位置信息
        response = JsonResponse({'code': 0, 'errmsg': 'ok'})
        response.set_cookie('username', username)
        # 合并cookie和用户的购物车数据
        merge_cart_cookie_to_redis(request, response)
        return response


# 退出模块
class LogoutView(View):
    def delete(self, request):
        logout(request)
        response = JsonResponse({'code': 0, 'errmsg': 'ok'})
        response.delete_cookie('username')
        return response


# 修改密码
class ChangePasswordView(LoginRequiredJSONMixin, View):
    def put(self, request):
        data = TransformJson(request)
        old_password = data.get('old_password')
        new_password = data.get('new_password')
        new_password2 = data.get('new_password2')
        # ***** 验证密码格式是否正确
        if not all([old_password, new_password, new_password2]):
            return JsonResponse({'code': 400,
                                 'errmsg': '缺少必传参数'})

        result = request.user.check_password(old_password)
        if not result:
            return JsonResponse({'code': 400, 'errmsg': '原始密码不正确'})

        if not re.match(r'^[0-9A-Za-z]{8,20}$', new_password):
            return JsonResponse({'code': 400, 'errmsg': '密码最少8位,最长20位'})

        if new_password != new_password2:
            return JsonResponse({'code': 400, 'errmsg': '两次输入密码不一致'})
        try:
            request.user.set_password(new_password)
            request.user.save()
        except Exception:
            return JsonResponse({'code': 400, 'errmsg': '修改密码失败'})

            # 清理状态保持信息
            logout(request)
            response = JsonResponse({'code': 0, 'errmsg': 'ok'})
            response.delete_cookie('username')

            # # 响应密码修改结果：重定向到登录界面
            return response


# 用户登录状态查询
class InfoView(LoginRequiredJSONMixin, View):
    def get(self, request):
        # request.user 已登录的用户信息
        info_data = {
            'username': request.user.username,
            'mobile': request.user.mobile,
            'email': request.user.email,
            'email_active': request.user.email_active,
        }
        return JsonResponse({'code': 0, 'errmsg': 'ok', 'info_data': info_data})


# 邮箱步骤：1. 接收请求，收获数据，保存邮件地址，返回响应
class EmailView(LoginRequiredJSONMixin, View):
    def put(self, request):
        data = TransformJson(request)
        email = data.get('email')
        if not email:
            return JsonResponse({'code': 400, 'errmsg': '没有输入邮箱'})
        elif not re.match('[a-z0-9][\w\.\-]*@[a-z0-9\-]+(\.[a-z]{2,5}){1,2}', email):
            return JsonResponse({'code': 400, 'errmsg': '邮箱格式不正确'})
        # 配置和引入发送邮箱的服务器
        from django.conf import settings
        from django.core.mail import send_mail

        # 获取数据，并更新
        user = request.user
        user.email = email
        user.save()
        token = Serializer_email_verify_token(request.user.id)
        send.delay(email, 'http://www.meiduo.site:8080/success_verify_email.html?token=' + token)
        # 发送邮件
        return JsonResponse({'code': 0, 'errmsg': 'ok'})


# 邮箱验证模块
class EmailVerifyView(View):
    def put(self, request):
        params = request.GET
        token = params.get('token')
        if token is None:
            return JsonResponse({'code': 400, 'errmsg': '参数不全'})
        user_id = UnSerializer_email_verify_token(token)
        user = User.objects.get(id=user_id)
        user.email_active = True
        user.save()
        return JsonResponse({'code': 0, 'errmsg': 'ok'})


# 新增地址
class AddressCreate(LoginRequiredJSONMixin, View):
    def post(self, request):
        data = TransformJson(request)
        receiver = data.get('receiver')
        province_id = data.get('province_id')
        city_id = data.get('city_id')
        district_id = data.get('district_id')
        place = data.get('place')
        mobile = data.get('mobile')
        tel = data.get('tel')
        email = data.get('email')

        user = request.user
        # ***** 添加验证(必传参数)
        # 地址id是否正确，详细地址长度，手机，固定电话，邮箱

        # 保存地址信息
        try:
            address = Address.objects.create(
                user=request.user,
                title=receiver,
                receiver=receiver,
                province_id=province_id,
                city_id=city_id,
                district_id=district_id,
                place=place,
                mobile=mobile,
                tel=tel,
                email=email
            )

            # 设置默认地址
            if not request.user.default_address:
                request.user.default_address = address
                request.user.save()
        except Exception as e:
            return JsonResponse({'code': 400, 'errmsg': '新增地址失败'})

        # 新增地址成功，将新增的地址响应给前端实现局部刷新
        address_dict = {
            "id": address.id,
            "title": address.title,
            "receiver": address.receiver,
            "province": address.province.name,
            "city": address.city.name,
            "district": address.district.name,
            "place": address.place,
            "mobile": address.mobile,
            "tel": address.tel,
            "email": address.email
        }

        # 响应保存结果
        return JsonResponse({'code': 0, 'errmsg': '新增地址成功', 'address': address_dict})


# 查看地址
class AddressView(LoginRequiredJSONMixin, View):
    def get(self, request):
        user = request.user
        addresses = Address.objects.filter(user=user, is_deleted=False)
        address_list = []
        # 获取默认地址id
        default_address_id = request.user.default_address_id
        for address in addresses:
            address_list.append({
                "id": address.id,
                "title": address.title,
                "receiver": address.receiver,
                "province": address.province.name,
                "city": address.city.name,
                "district": address.district.name,
                "place": address.place,
                "mobile": address.mobile,
                "tel": address.tel,
                "email": address.email
            })
        return JsonResponse(
            {'code': 0, 'errmsg': 'ok', 'addresses': address_list, 'default_address_id': default_address_id})


# 修改和删除地址
class UpdateDestroyAddressView(LoginRequiredJSONMixin, View):
    def put(self, request, address_id):
        data = TransformJson(request)
        receiver = data.get('receiver')
        province_id = data.get('province_id')
        city_id = data.get('city_id')
        district_id = data.get('district_id')
        place = data.get('place')
        mobile = data.get('mobile')
        tel = data.get('tel')
        email = data.get('email')
        # ***** 验证参数

        #
        try:
            Address.objects.filter(id=address_id).update(
                user=request.user,
                title=receiver,
                receiver=receiver,
                province_id=province_id,
                city_id=city_id,
                district_id=district_id,
                place=place,
                mobile=mobile,
                tel=tel,
                email=email
            )
        except Exception:
            return JsonResponse({'code': 400, 'errmsg': '更新地址失败'})

            # 构造响应数据
        address = Address.objects.get(id=address_id)
        address_dict = {
            "id": address.id,
            "title": address.title,
            "receiver": address.receiver,
            "province": address.province.name,
            "city": address.city.name,
            "district": address.district.name,
            "place": address.place,
            "mobile": address.mobile,
            "tel": address.tel,
            "email": address.email
        }

        # 响应更新地址结果
        return JsonResponse({'code': 0, 'errmsg': '更新地址成功', 'address': address_dict})

    def delete(self, request, address_id):
        """删除地址"""
        try:
            # 查询要删除的地址
            address = Address.objects.get(id=address_id)

            # 将地址逻辑删除设置为True
            address.is_deleted = True
            address.save()
        except Exception as e:
            return JsonResponse({'code': 400, 'errmsg': '删除地址失败'})

        # 响应删除地址结果
        return JsonResponse({'code': 0, 'errmsg': '删除地址成功'})


# 设置为默认地址
class DefaultAddressView(LoginRequiredJSONMixin, View):
    """设置默认地址"""

    def put(self, request, address_id):
        """设置默认地址"""
        try:
            # 接收参数,查询地址
            address = Address.objects.get(id=address_id)

            # 设置地址为默认地址
            request.user.default_address = address
            request.user.save()
        except Exception as e:
            logger.error(e)
            return JsonResponse({'code': 400, 'errmsg': '设置默认地址失败'})

        # 响应设置默认地址结果
        return JsonResponse({'code': 0, 'errmsg': '设置默认地址成功', 'default_address_id': address_id})


# 修改地址标题
class UpdateTitleAddressView(LoginRequiredJSONMixin, View):
    def put(self, request, address_id):
        """设置地址标题"""
        # 接收参数：地址标题
        json_dict = TransformJson(request)
        title = json_dict.get('title')

        try:
            # 查询地址
            address = Address.objects.get(id=address_id)

            # 设置新的地址标题
            address.title = title
            address.save()
        except Exception as e:
            logger.error(e)
            return JsonResponse({'code': 400, 'errmsg': '设置地址标题失败'})

        # 4.响应删除地址结果
        return JsonResponse({'code': 0, 'errmsg': '设置地址标题成功'})


# 登录用户浏览记录
# 利用redis list实现
class UserHistoryView(LoginRequiredJSONMixin, View):
    def post(self, request):
        user = request.user
        data = TransformJson(request)
        sku_id = data.get('sku_id')
        try:
            sku = SKU.objects.get(id=sku_id)
        except SKU.DoesNotExist:
            return JsonResponse({'code': 400, 'errmsg': '没有此商品'})
        redis_cli = get_redis_connection('history')
        # 去重
        redis_cli.lrem('history_%s' % user.id, 0, sku_id)
        # 添加元素
        redis_cli.lpush('history_%s' % user.id, sku_id)
        # 让列表只保留5条元素
        redis_cli.ltrim('history_%s' % user.id, 0, 4)
        return JsonResponse({'code': 0, 'errmsg': 'ok'})

    def get(self, request):
        """获取用户浏览记录"""
        # 获取Redis存储的sku_id列表信息
        redis_conn = get_redis_connection('history')
        sku_ids = redis_conn.lrange('history_%s' % request.user.id, 0, 4)

        # 根据sku_ids列表数据，查询出商品sku信息
        skus = []
        for sku_id in sku_ids:
            sku = SKU.objects.get(id=sku_id)
            skus.append({
                'id': sku.id,
                'name': sku.name,
                'default_image_url': sku.default_image.url,
                'price': sku.price
            })

        return JsonResponse({'code': 0, 'errmsg': 'OK', 'skus': skus})
