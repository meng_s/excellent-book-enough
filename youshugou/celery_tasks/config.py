# 配置信息：key = value
from datetime import timedelta

from celery.schedules import crontab

broker_url = "redis://127.0.0.1:6379/15"

result_backend = 'redis://127.0.0.1:6379/8'

task_serializer = 'json'
result_serializer = 'json'
accept_content = ['json']
# 时区设置
timezone = 'Asia/Shanghai'
# 过期时间，秒
result_expires = 60 * 60 * 24

worker_hijack_root_logger = False

# 导入任务所在文件
imports = [
    'apps.contents.crons',
]

# 需要执行任务的配置
beat_schedule = {
    'generic_index': {
        # 具体需要执行的函数
        # 该函数必须要使用@app.task装饰
        'task': 'apps.contents.crons.generic_index',
        # 定时时间
        # 每分钟执行一次，不能为小数
        'schedule': crontab(minute='*/1'),
        # 'schedule': timedelta(seconds=10),
        # 或者这么写，每小时执行一次
        # "schedule": crontab(minute=0, hour="*/1")
        # 执行的函数需要的参数
        'args': ()
    },
}
