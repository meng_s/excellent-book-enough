from django.core.mail import send_mail

from youshugou import settings
from youshugou.settings import CORS_ORIGIN_WHITELIST
from celery_tasks.main import app


@app.task
def send(to_email, verify_url):
    subject = '注册优书够验证信息'  # 主题
    message = "邮件内容"
    sender = settings.EMAIL_FROM  # 发送邮箱，已经在settings.py设置，直接导入
    receiver = [to_email]  # 目标邮箱
    # 发送html格式
    html_message = '<p>尊敬的用户您好！</p>' \
                   '<p>感谢您使用美多商城。</p>' \
                   '<p>您的邮箱为：%s 。请点击此链接激活您的邮箱：</p>' \
                   '<p><a href="%s">%s<a></p>' % (
                       to_email, verify_url,verify_url)
    send_mail(subject, message, sender, receiver, html_message=html_message)
