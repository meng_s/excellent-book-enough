import os
from celery import Celery
# 启动命令 celery -A celery_tasks.main  worker -l info -P eventlet
# celery -A celery_tasks.main beat
# celery -A celery_task.main worker
# 为celery设置Django的运行环境
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'youshugou.settings')

# 参数设置
app = Celery('celery_tasks')

# 设置broker，痛过加载配置文件
app.config_from_object('celery_tasks.config')

# celery检测指定包任务(参数是列表，列表中元素是tasks的路径)
app.autodiscover_tasks(['celery_tasks.sms', 'celery_tasks.email'])
