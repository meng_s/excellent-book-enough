# 生产者----任务
# 函数必须让celery的实例的task装饰器装饰
# 需要celery 自动检测指定包的任务
from libs.yuntongxun.sms import CCP
from celery_tasks.main import app


@app.task
def celery_send_sms_code(mobile, code):
    # CCP().send_template_sms(to,[code,expire],1)
    CCP().send_template_sms(mobile, [code, 5], 1)
