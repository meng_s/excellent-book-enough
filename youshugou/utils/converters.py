from django.urls import converters


# 用户名是否重复
class UsernameConverters:
    regex = '[a-zA-Z0-9_-]{5,20}'

    # 返回value 不能转换类型
    def to_python(self, value):
        return value

    def to_url(self, value):
        return value


# 判断验证码是否正确
class UUIDConverter:
    regex = '[\w-]+'

    def to_python(self, value):
        return value

class MobileConverter:
    regex = '1[345789]\d{9}'

    def to_python(self, value):
        return value
    def to_url(self, value):
        return value