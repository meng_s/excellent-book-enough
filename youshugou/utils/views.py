from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse


# 用户登录状态查询
class LoginRequiredJSONMixin(LoginRequiredMixin):
    def handle_no_permission(self):
        return JsonResponse({'code': 0, 'message': '没有登录'})
